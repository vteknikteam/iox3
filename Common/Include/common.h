/**************************************************************************************************
 * GENERAL HEADER FILE
 */

#ifndef _GENERAL_H_
#define _GENERAL_H_

#define USE_UART	1

/*
	Includes
*/

#define TRUE 		1
#define FALSE		0
#define ON			1
#define OFF			0

#define	EXT		7
#define	INT		8

typedef unsigned	char 			tU8;
typedef unsigned	char 			byte;
typedef unsigned	char 			BYTE;
typedef signed 	char			tS8;

typedef unsigned	int			tU16;
typedef unsigned	int			word;
typedef signed		int			tS16;

typedef unsigned long			tU32;
typedef signed long				tS32;

/*
#define LOW_BYTE(x)   ((tU8)x)
#define HIGH_BYTE(x)  ((tU8)(x>>8))
*/

#define LOW_BYTE(x)   (x & 0x00ff)
#define HIGH_BYTE(x)  ((x>>8) & 0x00ff)

/* I2C Chip adresses: */

#define  CHIP0ADR    0x40
#define  CHIP1ADR    0x42
#define  CHIP2ADR    0x44
#define  CHIP3ADR    0x46
#define  CHIP4ADR    0x48
#define  CHIP5ADR    0x4A
#define  CHIP6ADR    0x4C
#define  CHIP7ADR    0x4E
#define	CHIP0aADR	0x70
#define	CHIP1aADR	0x72
#define	CHIP2aADR	0x74
#define	CHIP3aADR	0x76
#define	CHIP4aADR	0x78
#define	CHIP5aADR	0x7A
#define	CHIP6aADR	0x7C
#define	CHIP7aADR	0x7E

/*
ID:s (sent from terminal)
0:		BUTTONS
1:		BUTTON MAKE
		BUTTON BrAKE
2-3:	T-volt

From maskin:
	0: LEDS
	1: SUMMER
	2-6:LCD-array
*/

// Destination M:
#define	ID_FROM_T0				0x10
#define	ID_TO_M0					0x10
#define	ID_TO_M0_V2				0x13

//speed info:
#define ID_SPEED_TO_M0			0x14

// Destination T:
#define	ID_FROM_M0				0x20
#define	ID_TO_T0					0x20
#define ID_TO_T0_V2				0x23

// Terminals:
#define	V1	1
#define	V2	2

// ABL TEST
#define	ID_ABLT_ADC0			0x21
#define	ID_ABLT_ADC1			0x22
#define	ID_ABLT_ADC2			0x23
#define	ID_ABLT_SET				0x30
#define	ID_ABLT_RESET			0x31

#define	ID_TEST_VECTOR_0				0x45	/* Device dependent */
#define	ID_TEST_RESPONSE_0			0x4A	/* Device dependent */
#define	ID_TEST_RESPONSE_1			0x4B	/* Device dependent */
#define	ID_TEST_RESPONSE_2			0x4C	/* Device dependent */

/* Variable definitions: */
#define	BV_SET		0
#define	TILT_TOL		1
#define	TILT_DLY		2
#define	TILT_GAIN	3
#define	HEIGHT_TOL	4
#define	HEIGHT_DLY	5

#define	HEIGHT_GAIN	6
#define	MIDDLE_DLY	7
#define	MIDDLE_GAIN	8
#define	EFTERLYFT	9
#define	BOMSLACK		10
#define	BV_INC		11
#define	BV_MIDDLE	12

#define	BYPASS_MODE	13
#define	MIN_OPEN		14
#define	AREASW_POL	15
#define	TRANSPARENS	16
#define	GAIN_FACTOR	17

#define	VxValve		18
#define	Spare1		19
#define	MAX_CURRENT	20

/* << MAXCHG >> */

#define	MAXCHG		21

#define	CURRENT_PWR	21
#define	VOLT_EL_T	22
#define	VOLT_EL_M	23
#define	VOLT_PWR		24

#define	AV_LEFT		25
#define	AV_RIGHT		26
#define	AV_MIDDLE	27
#define	SUB_MODE_P	28

#define	CALIB_SEQ	29
#define	Spare3		30
#define	Spare4		31
#define	AV_MIDDLE2	32
#define	AV_SPARE_1	33
#define	AV_SPARE_2	34
#define	ERROR_CNT_0	35
#define	ERROR_CNT_1	36
#define	ERROR_CNT_2	37
#define	ERROR_CNT_3	38
#define	ERROR_CNT_4	39
#define	ERROR_CNT_5	40
#define	M_OUTPUTS	41
#define	M_INPUTS		42
#define	ADC00			43
#define	ADC01			44
#define	ADC02			45
#define	ADC03			46
#define	ADC04			47
#define	ADC05			48
#define	ADC06			49
#define	ADC07			50
#define	ADC08			51
#define	DUMMY_1		53
#define	MAXVAR		DUMMY_1+1

#define	OP_NORMAL		1
#define	OP_PAR_CHANGE	2
#define	OP_CHANGE		3
#define	OP_SHOW			4
#define	OP_T_TEST		5
#define	OP_M_TEST		6
#define	SM_SIDA_V		7
#define	SM_SIDA_H		8
#define	SM_TILT			9
#define	SM_BOM			10

#define	SHOW_TC		15

typedef	struct
{
	int	val;
	int	old_val;
	int	loaded;
} vartyp;

typedef	struct
{
	tU16	tag;
	tS16	minval;
	tS16	defval;
	tS16	maxval;
} varlim_t;

#define	DECIMALS_0			0x0000
#define	DECIMALS_1			0x0001
#define	DECIMALS_2			0x0002
#define	DECIMALS_3			0x0004
#define	COLON					0x0008
#define	CHANGE_ALLOWED		0x0010
#define	ZEROING_ALLOWED	0x0020

/* Not used for in itialization: */
#define	LEADING_ZERO		0x0040
#define	COLON_BLINK			0x0080

#define	CH_0_LZ	(CHANGE_ALLOWED | DECIMALS_0 | LEADING_ZERO)
#define	CH_2_LZ	(CHANGE_ALLOWED | DECIMALS_2 | LEADING_ZERO)
#define	CH_0		(CHANGE_ALLOWED | DECIMALS_0)
#define	CH_1		(CHANGE_ALLOWED | DECIMALS_1)
#define	CH_2		(CHANGE_ALLOWED | DECIMALS_2)
#define	NOCH_0	(DECIMALS_0)
#define	NOCH_1	(DECIMALS_1)
#define	NOCH_2	(DECIMALS_2)
#define	NOCH_3	(DECIMALS_3)
#define	ZNOCH_0	(LEADING_ZERO | DECIMALS_0)
#define	ZNOCH_1	(LEADING_ZERO | DECIMALS_1)
#define	ZNOCH_2	(LEADING_ZERO | DECIMALS_2)
#define	ZNOCH_3	(LEADING_ZERO | DECIMALS_3)
#define	ZERO_0	(ZEROING_ALLOWED)


#ifdef	VAR_GLOBALS
const varlim_t	varlim[] = {
/* antal decimaler+Leading Zero suppression, min,default,max */
/* 0-5: */
/* BV_SET]		*/	{CH_2_LZ,	0,150,9999},
/* TILT_TOL]	*/	{CH_0,		0,5,99},
/* TILT_DLY]	*/	{CH_1,		0,40,999},
/* TILT_GAIN]	*/	{CH_0,		0,10,999},
/* HEIGHT_TOL]	*/	{CH_0,		0,5,99},
/* HEIGHT_DLY]	*/	{CH_1,		0,20,999},
/* 6-12: */
/* HEIGHT_GAIN]*/	{CH_0,		0,50,999},
/* MIDDLE_DLY]	*/	{CH_1,		0,20,999},
/* MIDDLE_GAIN]*/	{CH_0,		0,45,999},
/* EFTERLYFT 	*/	{CH_1,		0,30,999},
/* BOMSLACK 	*/	{CH_0,		0,20,999},
/* BV_INC */		{CH_2,			0,15,999},
/* BV_MIDDLE]	*/	{CH_2,		0,150,9999},
/* 13-17: */
/* BYPASS MODE */	{CH_0,		0,1,3},
/* MIN_OPEN		*/	{CH_1,		0,0,100},
/* AREASW_POL	*/	{CH_0,		0,1,3},
/* TRANSPARENS	*/	{CH_0,		0,1111,2222},
/* GAIN_FACTOR	*/	{CH_0,		0,100,9999},
/* 18-20: */
/* VxValve		*/	{CH_0,		0,0,0x7fff},
/* Spare 1		*/	{CH_0,		0,100,9999},
/* MAX_CURRENT	*/	{CH_1,		0,100,9999},/* <== MAXCHG 20130614 */
 
/* 21-25: */
/* CURRENT_PWR]*/	{ZNOCH_1,	0,0,9999},/* <== MAXCHG 20141202 */
/* VOLT_EL_T]	*/	{ZNOCH_1,	0,0,9999},
/* VOLT_EL_M]	*/	{ZNOCH_1,	0,0,9999},
/* VOLT_PWR]	*/	{ZNOCH_1,	0,0,9999},
/* AV_LEFT]	*/		{ZNOCH_2,	0,0,9999},
/* 26-30: */
/* AV_RIGHT]	*/	{ZNOCH_2,	0,0,9999},
/* AV_MIDDLE]	*/	{ZNOCH_2,	0,0,9999},
/* SUB_MODE_P */	{CH_0,		0,0,9999},
/* CALIB_SEQ */	{CH_0,		0,0,9999},
/* Free */			{CH_0,		0,0,9999},
/* 31-35 */
/* Free */			{CH_0,		0,0,9999},
/* AV_MIDDLE2]	*/	{ZNOCH_2,	0,0,9999},
/* AV_SPARE_1 */  {ZERO_0,		0,0,9999},
/* AV_SPARE_2 */  {ZERO_0,		0,0,9999},
/* ERROR_CNT_0]*/	{ZERO_0,		0,0,9999},
/* 36-40 */
/* ERROR_CNT_1]*/	{ZERO_0,		0,0,9999},
/* ERROR_CNT_2]*/	{ZERO_0,		0,0,9999},
/* ERROR_CNT_3]*/	{ZERO_0,		0,0,9999},
/* ERROR_CNT_4]*/	{ZERO_0,		0,0,9999},
/* ERROR_CNT_5]*/	{ZERO_0,		0,0,9999},
/* 41-45 */
/* M_OUTPUTS*/		{ZERO_0,		0,0,9999},
/* M_INPUTS	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/*46-50 */
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/*51-55 */
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},
/* Dummies	*/		{ZERO_0,		0,0,9999},

};

				
#else
extern	const varlim_t	varlim[];
#endif

typedef struct
{
	tU16	msgID;
	tU8 	dstadr;
	tU8 	srcadr;
	tU8	dlc;
	tU8	msg[8];
}CANMSG_T;


/* ERROR FLAGS */
#define NO_ERROR		0

/* STATUS FLAGS */
#define NOT_ACTIVE		0	/* Waiting for ID_MODULE_FIND message from master */
#define NOT_CONFIGURED	1	/* Master found, not yet configured */
#define CONFIG_ACTIV	1	/* Modul active in config menu -> flashing Yellow LED */
#define CONFIG_OK		2	/*  */

#endif
