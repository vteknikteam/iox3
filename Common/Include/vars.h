/***********************************************************************
 * VARS HEADER FILE
 *----------------------------------------------------------------------
 *----------------------------------------------------------------------
 * CREATED BY: FREDRIK MARTINSSON
 * 20050308
 ***********************************************************************/
#ifndef _VARS_H
#define _VARS_H

#ifdef VAR_GLOBALS
#define VAR_EXT
#else
#define VAR_EXT extern
#endif

#include	"\IAR\Can_ID.h"

/* DEFINE ADC CHANNELS */
#define NUM_OF_ADC 		4

#define AN_IN1			0
#define AN_IN2			1
#define AN_IN3			2
#define AN_IN4			3

void TSETOUTP(void);
void TE100ms (void);
void TE500ms (void);
void TPROCESS(void);
void TREGLER (void);
void TCOMM	 (void);

/* void Tpower (void); */

/* EE-prom */

void savevar(void);

#define	SETOUTP	0
#define	E100ms 	1
#define	E500ms	2
#define	PROCESS	3
#define	REGLER	4
#define	COMM		5

#define	NOTASKS 6

#define	BPSETOUTP	10
#define	BPE100ms 	10
#define	BPE500ms		10
#define	BPPROCESS	14
#define	BPREGLER		11
#define	BPCOMM		12

/* TIMER VARIABLES */

VAR_EXT tU8	EV_10ms_cnt,
				EV_100ms_cnt,
				EV_500ms_cnt,
				EV_minute_cnt,
				start_delay,
				ADC_channel,
				ADC_timer,
				lcdbytes[4], adjustinc, IO_alive, IO_dead;

/* SYSTEM VARIABLES */
VAR_EXT bit	pwrFail_bit,
				birelay,
				test_bit, motorA, motorB, Flyt_flag, Flyt_req, Display_vridposflag, Display_trycklimflag,
				aktivera_flag, passivera_flag, vridningsAuto, svansAuto, FS_Auto, BS_Auto, flash, bp_toggle,
				PilNerKnapp, PilUppKnapp, SetKnapp, ResetKnapp, PlusKnapp_ff, MinusKnapp_ff,
				PlusKnapp, MinusKnapp, spec_knapp, hregflag,
				biarea, ttt, bitti, sw_11_ff, hjulflag;

VAR_EXT xdata	tU16	ADC_value[6];

VAR_EXT idata	tU8	ch, error, keepalive_timer, lcd_dec, led_togg, Err_POS;

VAR_EXT idata	tU16	eeadr,
							lcd_val,
							pvarix,
							hjulpc, hjultim, testint, hast_inv;

VAR_EXT xdata 	tS16	decitim;
VAR_EXT xdata 	tU16	old_lcd_dec, old_lcd_val, adjust_val, decameter, old_faktor;
VAR_EXT xdata	tU32	UtInt1, old_UtInt1;
VAR_EXT idata	tU32	hjulack;

/* COMMUNICATION VARIABLES */

VAR_EXT xdata	tU8 fileChksum;
VAR_EXT xdata	tU8 endian;
VAR_EXT xdata	tU8 strnum;					/*holds current string while sending*/
VAR_EXT xdata	tU8 oddaddr;
VAR_EXT xdata	tU16	var[MAXVAR], old_var[MAXVAR];
VAR_EXT xdata	tU8	var_flags[MAXVAR];
VAR_EXT xdata	tU8	in_value[8];


/* TASK VARIABLES */

VAR_EXT bit 	sys_start, BIACK, cantxlock;

VAR_EXT data tU8	tprio, tt, tptr, prio[NOTASKS], oldP0, oldP1, tconfig, minut, tcnt, FS_billh, BS_billh,FS_vridpos, P3_in, FS_inccnt, FS_deccnt,
						flash_timer, led_timer, Pil_to, Set_to, Reset_to, lcd_flash, VAL_CHG, OLD_VAL_CHG;

VAR_EXT xtU8		VRID_BV, ee_checksum,
						BS_inccnt, BS_deccnt, FS_autoflag, BS_autoflag, data_0, data_1, data_2, data_3, data_4, data_5,
						chg_to, Flyt_to, hjul_to, vridpos_BV, aktiv_to, passiv_to, js, old_js, FS_Lyft_to, BS_Lyft_to,
						old_0, old_1, old_2, old_3, old_4, old_5, SVANS_Lyft_to;

VAR_EXT tS8		varix, old_varix, pos, setopt;

VAR_EXT xtU16 VRID_to, hast_timeOut; /*VRID_to andrad fr�n VAR_EXT xtU8, lagt till hast_timeOut (begr�nsad uppr�knning iE100ms task) 081009 //Martin*/

#if 0
/* Device serial number variables */
VAR_EXT tU8 device_open;
VAR_EXT tU8 device_serial_number[8];
VAR_EXT tU32 device_serial_number_long;
VAR_EXT tU8 device_serial_checksum;
VAR_EXT tU8 device_serial_checksum_ok;
VAR_EXT tU8 device_serial_new_write_ok;
#define SERIAL_NUMBER_EEADDRESS		0
#define SERIAL_CHECKSUM_EEADDRESS		8
#endif
#endif

/* Expansion module variables */
#define IOCM_TIMEOUT	20 /* 2 second timeout */

VAR_EXT CANMSG_T canmsg, outmsg;

#define FLASH_TC_2HZ	5







