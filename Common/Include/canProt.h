/***********************************************************************
 * CAN PROTOCOLL
 *----------------------------------------------------------------------
 *----------------------------------------------------------------------
 * CREATED BY: FREDRIK MARTINSSON
 * 20050708
 ***********************************************************************/
#ifndef _CANPRO_H
#define _CANPRO_H

/*

	CAN PROTOCOL SETTINGS and ID�s

*/


/* CAN ID:s */


/* Alarms */
#define TYPE_ERROR		1
#define TYPE_WARNING		2
#define TYPE_INFO			3

#include	"/IAR/Can_ID.h"

tU8 can_ID_AGMSTART(void);
tU8 can_ID_SET_VAR(tU8 dstadr, tU16 var_ix, tS32 var_val);
tU8 can_ID_REQ_var(tU8 dstadr, tU16 var_ix);
tU8 can_ID_DEFA_VAR(tU8 dstadr, tU16 var_ix);
tU8 can_ID_DEFB_VAR(tU8 dstadr, tU16 var_ix);
tU8 can_ID_DEFC_VAR(tU8 dstadr, tU16 var_ix);
tU8 can_ID_GOTO(tU8 dstadr, tU16 menu_id);
tU8 can_ID_KEEPALIVE(tU8 dstadr);
tU8 can_ID_I2C_READ(tU8 dstadr, tU8 i2c_adr, tU8 bytes_to_read);
tU8 can_ID_PROG_OPEN(tU8 dstadr, tU16 version_id, tU8 file_checksum, tU8 language, tU16 maxvar);
tU8 can_ID_PROG_START(tU8 dstadr, tU16 str_size, tU16 item_size, tU16 bmp_size);

tU8 SET_ALARM(tU8 type, tU16 alarm_ix, tU8 alarm, tU8 time, tU8 beep);
tU8 CLR_ALARM (tU8 type, tU16 alarm_ix, tU16 alarm);
tU8 can_ID_BEEP(tU8 dstadr, tU8 b);


#endif


